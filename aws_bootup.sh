sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm -y
sudo dnf install zip atop sysbench git python3-pip podman python38 -y
sudo pip3 install --upgrade pip
sudo pip3 install git+https://github.com/cloud-bulldozer/benchmark-wrapper.git

unzip ./benchmark-wrapper.zip

sudo dnf install python3-devel -y

sudo bash benchmark-wrapper/setup-wrapper-env.sh
sudo python3.8 -m pip install bw-env/benchmark-wrapper/
git clone https://gitlab.com/pts-demo/pts
