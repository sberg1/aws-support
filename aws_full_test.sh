#!/bin/bash

#TODO use args?
#Take instance type as variable
instance_type="t4g.medium"
export clustername=$instance_type
export test_user=$USER
echo $instance_type >> instance_type.txt
region="us-east-2"
#ARM centos 8
image_id="ami-035734c938e7da7af"

#Create new instance with ssh keys and port 22 open (security group sberg-dev) if needed
#else start old one
#parse instance id
instance_id=$(aws ec2 run-instances --image-id $image_id --count 1 --instance-type $instance_type --key-name aws-sberg --security-group-ids sg-0d5020c37540cde8e | jq '.Instances[0].InstanceId')
instance_id="${instance_id%\"}"
instance_id="${instance_id#\"}"
echo $instance_id


#monitor status of instance
instance_status=$(aws ec2 describe-instances --instance-ids $instance_id | jq '.Reservations[0].Instances[0].NetworkInterfaces[0].Attachment.Status')
instance_status="${instance_status%\"}"
instance_status="${instance_status#\"}"
echo $instance_status


#wait for instance to come online
while [ $instance_status != "attached" ]; do
    instance_status=$(aws ec2 describe-instances --instance-ids $instance_id | jq '.Reservations[0].Instances[0].NetworkInterfaces[0].Attachment.Status')
    instance_status="${instance_status%\"}"
    instance_status="${instance_status#\"}"
    echo $instance_status
    sleep 2
done

#wait for instance to be ready for ssh
instance_ready=$(aws ec2 describe-instance-status --instance-ids $instance_id | jq '.InstanceStatuses[0].InstanceStatus.Status')
instance_ready="${instance_ready%\"}"
instance_ready="${instance_ready#\"}"
while [ $instance_ready != "ok" ]; do
    instance_ready=$(aws ec2 describe-instance-status --instance-ids $instance_id | jq '.InstanceStatuses[0].InstanceStatus.Status')
    instance_ready="${instance_ready%\"}"
    instance_ready="${instance_ready#\"}"
    echo $instance_ready
    sleep 5
done

#once up, parse ip address
instance_ip=$(aws ec2 describe-instances --instance-ids $instance_id | jq '.Reservations[0].Instances[0].NetworkInterfaces[0].Association.PublicDnsName')
instance_ip="${instance_ip%\"}"
instance_ip="${instance_ip#\"}"

echo $instance_ip

#Transfer scripts and conf
scp -i $HOME/Downloads/aws-sberg.pem -o StrictHostKeyChecking=no -r ./* centos@$instance_ip:~

#Set up instance env
ssh -i $HOME/Downloads/aws-sberg.pem -o StrictHostKeyChecking=no centos@$instance_ip './aws_bootup.sh'

#Run test suite
ssh -i $HOME/Downloads/aws-sberg.pem -o StrictHostKeyChecking=no centos@$instance_ip './test_all.sh'

#copy test results back to host
scp -i $HOME/Downloads/aws-sberg.pem -o StrictHostKeyChecking=no -r centos@$instance_ip:~/results ./results

#push results to elasticsearch
podman run -v ./results/:/project:z -e es=10.16.31.52:9200 -it registry.gitlab.com/pts-demo/pts/benchmark-wrapper-sysbench:amd64 bash -c "run_snafu --tool archive --archive-file /project/*.archive"

#Delete or stop instance
#ssh -i $HOME/Downloads/aws-sberg.pem -o StrictHostKeyChecking=no centos@$instance_ip
aws ec2 terminate-instances --instance-ids $instance_id
rm instance_type.txt
