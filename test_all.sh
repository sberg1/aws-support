#!/bin/bash

export test_user=$USER

export clustername=$(cat ./instance_type.txt)

mkdir ~/results
cd ~/results

$HOME/pts/workload.sh sysbench $HOME/cpu_test.conf

$HOME/pts/workload.sh sysbench $HOME/memory_test.conf

$HOME/pts/workload.sh sysbench $HOME/mutex_test.conf

$HOME/pts/workload.sh sysbench $HOME/threads_test.conf

$HOME/pts/workload.sh fileio --file-total-size=10G prepare
$HOME/pts/workload.sh sysbench $HOME/fileio_test.conf
$HOME/pts/workload.sh fileio --file-total-size=10G cleanup

cd ~
